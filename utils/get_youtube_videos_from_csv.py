from __future__ import unicode_literals
import os
from time import time
from sys import path
from multiprocessing.dummy import Pool

from unicodecsv import reader

from youtube_dl import YoutubeDL

ROOT_DIR = os.path.dirname(os.path.abspath(__file__))
path.append(ROOT_DIR + '/../')  # noqa
from app import constants

query = raw_input()

start_time = time()
yt_urls = []

ydl_options = {
	'quiet': True,
	'outtmpl': constants.DATA_DIR + 'tmp/' + query + '/%(id)s',
	'format': 'worst',
	'socket-timeout': 60,
	'download_archive': constants.DATA_DIR + 'downloads.txt'
}


def download_video(url):
	with YoutubeDL(ydl_options) as ydl:
		try:
			ydl.download([url])
		except Exception as e:
			print(url + str(e))


with open(constants.DATA_DIR + query + '.csv', 'r') as f:
	csv_reader = reader(f)

	is_header = True
	for row in csv_reader:
		if is_header:
			is_header = False
			continue

		yt_urls.append(row[1])


pool = Pool(16)
pool.map(download_video, yt_urls)

end_time = time()
print(end_time-start_time)
