import os
from time import time
from sys import path

from unicodecsv import writer

ROOT_DIR = os.path.dirname(os.path.abspath(__file__))
path.append(ROOT_DIR + '/../')  # noqa
from app import constants
from app.ndvr import youtube as yt

query = raw_input()
videos = []

start_time = time()

page_token = None
for i in range(0, 4):
	page_token = yt.get_videos(query, videos, page_token)
	if not page_token:
		break

# save results to csv and html file
html = '<html> <meta charset="UTF-8">'
html += '<title>' + query + '</title>'
html += '<body> <table>'
with open(constants.DATA_DIR + query + '.csv', 'w') as f:
	csv_writer = writer(f)
	top_row = ['VideoId', 'Url', 'Title', 'DefaultThumb',
			   'ChannelId', 'PublishedAt', 'Duration']
	csv_writer.writerow(top_row)

	for v in videos:
		# csv row
		row = [v['VideoId'], v['Url'], v['Title'], v['DefaultThumb'],
			   v['ChannelId'], v['PublishedAt'], v['Duration']]
		csv_writer.writerow(row)

		# html row
		html += '<tr>'
		html += '<td>' + row[0] + '</td>'
		html += '<td><a href="' + row[1] + '">' + row[2][:30] + '</a></td>'
		html += '<td><img src="' + row[3] + '" /></td>'
		duration = str(int(row[6])//60) + ':' + str(int(row[6])%60)
		html += '<td>' + duration + '</td>'
		path = '../thumbnails/' + query + '/' + row[0]
		html += '<td><img src="' + path + '1.jpg" /></td>'
		html += '<td><img src="' + path + '2.jpg" /></td>'
		html += '<td><img src="' + path + '3.jpg" /></td>'
		html += '</tr>'

# write html file
with open(constants.DATA_DIR + '/html/' + query + '.html', 'w') as f:
	f.write(html.encode('utf8'))

end_time = time()
print(end_time-start_time)
