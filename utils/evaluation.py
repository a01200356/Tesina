import os
from sys import path
from time import time
from multiprocessing.dummy import Pool
from functools import partial
from json import load

from unicodecsv import reader
from numpy import array, arange, interp
import matplotlib
matplotlib.use("Agg")
import matplotlib.pyplot as plt

ROOT_DIR = os.path.dirname(os.path.abspath(__file__))
path.append(ROOT_DIR + '/../')  # noqa
from app import constants
from app.ndvr import spatiotemporal as st

use_storyboards = True
queries = [
            "charlie chaplin eating machine",
            "corn on drill challenge hair",
            "dimitri finds out",
            "haruhi suzumiya god knows",
            "He-Man What's Going On",
            "Hippo The Lion Sleeps Tonight",
            "Lady Pizza",
            "michael jackson superbowl 1993",
            "Napoleon Dynamite Dance",
            "star wars the phantom menace opening crawl"
          ]

def save_plot(name, recalls, precisions):
    try:
        plt.xlim(0, 1.05)
        plt.ylim(0, 1.05)
        plt.xticks(arange(0, 1.1, 0.1))
        plt.yticks(arange(0, 1.1, 0.1))
        #if use_storyboards:
        #    label = "Metadata"
        #else:
        #    label = "Videos"
        #precision_plot = plt.plot(recalls, precisions, label=label)
        precision_plot = plt.plot(recalls, precisions)
        plt.suptitle(name.title())
        plt.xlabel('Recall')
        plt.ylabel('Precision')

        filename = constants.DATA_DIR + 'results/' + name + 'all.png'
        if use_storyboards:
            filename += ' sb-t.png'
        else:
            filename += ' vd.png'
        plt.legend(loc='upper right')
        plt.savefig(filename)
        plt.close()
    except Exception as e:
        print(e)


recalls = arange(0, 1, 0.01)
for weight in range(0, 2):
    use_storyboards = False
    if weight == 1:
        use_storyboards = True
    precisions = []
    mean_average_precision = 0
    for query in queries:
        query_id = None
        col_names = []
        truth = {}
        videos = []
        print(query)
        start_time_csv = time()
        with open(constants.DATA_DIR + query + '.csv', 'r') as f:
            csv_reader = reader(f)

            is_header = True
            for row in csv_reader:
                if is_header:
                    is_header = False
                    for col in row:
                        col_names.append(col)
                else:
                    video = {}
                    for i in range(0, len(col_names)):
                        video[col_names[i]] = row[i]
                    videos.append(video)

                    if not query_id:
                        query_id = row[0]
                        #print(query_id)
                        continue

                    is_dup = int(row[-2]) and \
                             "Loop" not in row[-1]
                    if not is_dup:
                        truth[row[0]] = False
                    else:
                        truth[row[0]] = row[-1].split(',')

        start_time_algo = time()
        if use_storyboards:
            pool = Pool(16)
            pool.map(partial(st.index_video, dirname=query), videos)
        else:
            #pool = Pool(16)
            #pool.map(partial(st.index_video, dirname=query, from_storyboard=False), videos)
            with open(constants.DATA_DIR + 'all_ids.json', 'r') as f:
                ids = load(f)
            for v_id in ids:
                if v_id not in truth:
                    truth[v_id] = False

        results = st.query_index(query_id, query, from_storyboard=use_storyboards, weight=0.7)

        end_time = time()
        """
        if use_storyboards:
            print(end_time-start_time_csv)
        else:
            print(end_time-start_time_algo)
        """

        recall = []
        precision = []
        rank = 1
        dups_found = 0
        false_pos = 0
        avg_precision = 0
        n_duplicates = float(reduce(lambda acum, is_dup: acum+int(not not is_dup), truth.values(), 0))
        for result in results:
            is_dup = truth.get(result[0], False)

            if is_dup:
                dups_found += 1
                recall.append(dups_found/n_duplicates)
                precision.append(dups_found/float(dups_found+false_pos))
                avg_precision += int(bool(is_dup)) * precision[-1]
                """
                for video in videos:
                    if video['VideoId'] == result[0]:
                        print(str(rank) + '\t' + \
                              '{:.4f}'.format(result[1]) + '\t' + \
                              result[0] + '\t' + str(is_dup) + '\t\t' + \
                              video['Duration'])
                        break
                """

                if recall[-1] == 1.0:
                    break
            else:
                false_pos += 1
                #print(str(rank) + '\t' + '{:.4f}'.format(result[1]) + '\t' + result[0] + '\t' + str(is_dup))
                """
                if recall[-1] < 0.8:
                    for video in videos:
                        if video['VideoId'] == result[0]:
                            print(str(rank) + '\t' + \
                                  '{:.4f}'.format(result[1]) + '\t' + \
                                  result[0] + '\t' + str(is_dup) + '\t' + \
                                  video['Duration'])
                            break
                """

            rank += 1

        avg_precision /= n_duplicates
        mean_average_precision += avg_precision
        print(avg_precision)
        #print

        precision = interp(recalls, recall, precision)
        recall = recalls
        #save_plot(query, recall, precision)
        precisions.append(precision)

    mean_average_precision /= len(queries)
    print(mean_average_precision)
    #save_plot("average precision", recall, array(precisions).mean(axis=0))
    #break
#plt.close()
