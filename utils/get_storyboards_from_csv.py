from pathlib import Path
from urllib import urlencode, unquote

from unicodecsv import reader
from requests import get

INFO_URL = 'https://www.youtube.com/get_video_info?video_id={VIDEO_ID}' \
           + '&asv=3&el=detailpage'


# download and save video's default storyboard
def get_storyboard(video_id, path):
    info_url = INFO_URL.format(VIDEO_ID=video_id)
    r = get(info_url)
    info = r.text
    pos = info.find('storyboard_spec=')
    if pos != -1:
        sb_urls = unquote(info[pos+16:].split('&')[0]).split('|')
        if len(sb_urls) >= 2:
            sb_url = sb_urls[0]
            sb_param = sb_urls[1]
            storyboard_url = sb_url.replace('$L', '0').replace('$N', 'default')
            storyboard_url += '?sigh=' + sb_param[sb_param.find('rs'):]
            storyboard_response = get(storyboard_url)
            with open(path + video_id + '.jpg', 'wb') as f:
                f.write(storyboard_response.content)


filename = input()
html = ''
with open('./data/' + filename + '.csv', 'r') as f:
    csv_reader = reader(f)

    header = True
    for row in csv_reader:
        if header:
            header = False
            continue
        video_id = row[0]
        sb_path = 'data/storyboards/' + filename + '/'
        storyboard_path = Path(sb_path)
        storyboard_path.mkdir(exist_ok=True)
        get_storyboard(video_id, sb_path)
