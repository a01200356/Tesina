from unicodecsv import reader

filename = raw_input()
html = ''
with open('./app/static/data/' + filename + '.csv', 'r') as f:
    csv_reader = reader(f)

    html += '<html> <meta charset="UTF-8">'
    html += '<title>' + filename + '</title>'
    html += '<body> <table>'
    is_header = True
    for row in csv_reader:
        html += '<tr>'

        if is_header:
            html += '<th>' + row[0] + '</th>'
            html += '<th>' + row[2] + '</th>'
            html += '<th>' + row[3] + '</th>'
            html += '<th>' + row[6] + '</th>'
            html += '<th>Thumb 1</th>'
            html += '<th>Thumb 2</th>'
            html += '<th>Thumb 3</th>'
            html += '</tr>'
            is_header = False
            continue

        html += '<td>' + row[0] + '</td>'
        html += '<td><a href="' + row[1] + '">' + row[2][:30] + '</a></td>'
        html += '<td><img src="' + row[3] + '" /></td>'
        time = str(int(row[6])//60) + ':' + str(int(row[6])%60)
        html += '<td>' + time + '</td>'
        path = '../thumbnails/' + filename + '/' + row[0]
        html += '<td><img src="' + path + '1.jpg" /></td>'
        html += '<td><img src="' + path + '2.jpg" /></td>'
        html += '<td><img src="' + path + '3.jpg" /></td>'

        html += '</tr>'

    html += '</table> </body>'

with open('./app/static/data/html/' + filename + '.html', 'w') as f:
    f.write(html.encode('utf8'))
