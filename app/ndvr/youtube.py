from os import path, stat, mkdir
from functools import partial
from time import time
from multiprocessing.dummy import Pool
from json import loads
from urllib import urlencode, unquote

from unicodecsv import writer
from requests import get

from app import constants
from app.ndvr import spatiotemporal as st

API_KEY = ''

YOUTUBE_API_URL = 'https://www.googleapis.com/youtube/v3/'
VIDEO_LIST_URL = YOUTUBE_API_URL \
                 + 'search?part=id&{QUERY}' \
                 + '&maxResults=50&safeSearch=none&regionCode=US' \
                 + '&type=video&key={API_KEY}'
VIDEO_DETAILS_URL = YOUTUBE_API_URL \
                    + 'videos?part=snippet%2C+contentDetails' \
                    + '&id={ID_LIST}&key={API_KEY}'
PAGE_PARAM = '&pageToken={PAGE_TOKEN}'
THUMBNAIL_URL = 'https://i1.ytimg.com/vi/{VIDEO_ID}/{THUMB_NUMBER}.jpg'
INFO_URL = 'https://www.youtube.com/get_video_info?video_id={VIDEO_ID}' \
           + '&asv=3&el=detailpage'

pool = Pool(16)
pool2 = Pool(16)


# download and save video's default storyboard
def get_storyboard(video_id, path):
    info_url = INFO_URL.format(VIDEO_ID=video_id)
    r = get(info_url)
    info = r.text
    pos = info.find('storyboard_spec=')
    if pos == -1:
        return False
    sb_urls = unquote(info[pos+16:].split('&')[0]).split('|')
    if len(sb_urls) < 2:
        return False

    sb_url = sb_urls[0]
    sb_param = sb_urls[1]
    if 'storyboard3' in sb_urls[0]:
        storyboard_url = sb_url.replace('$L', '0').replace('$N', 'default')
    else:
        storyboard_url = sb_url.replace('$N', 'storyboard_low')
    storyboard_url += '?sigh=' + sb_param[sb_param.find('rs'):]

    storyboard_response = get(storyboard_url)
    if storyboard_response.status_code != 200:
        return False

    with open(path + video_id + '.jpg', 'wb') as f:
        f.write(storyboard_response.content)
    return True


# download and save video_id's thumbnail i
def get_thumbnail(i, video_id, path):
    thumb_url = THUMBNAIL_URL.format(VIDEO_ID=video_id, THUMB_NUMBER=i)
    r = get(thumb_url)
    with open(path + video_id + str(i) + '.jpg', 'wb') as f:
        f.write(r.content)


# download and save video_id's thumnails and storyboard
# returns video_id if storyboard could not be retrieved
def get_all_images(video_id, th_path, sb_path):
    get_storyboard(video_id, sb_path)
    #if get_storyboard(video_id, sb_path):
    if True:
        pool2.map(partial(get_thumbnail, video_id=video_id, path=th_path),
                  [1, 2, 3])
    else:
        return video_id


# extract video details
def extract_video_details(item):
    v = {}
    v_id = item['id']
    v['VideoId'] = v_id
    v['Url'] = 'https://www.youtube.com/watch?v=' + v_id
    v['ChannelId'] = item['snippet']['channelId']
    v['PublishedAt'] = item['snippet']['publishedAt']
    v['Title'] = item['snippet']['title']
    v['DefaultThumb'] = item['snippet']['thumbnails']['default']['url']

    # extract video durations
    duration = 0
    duration_str = item['contentDetails']['duration'][1:]
    duration_str = duration_str.replace('T', '')

    time_units = [('W', 604800),
                  ('D', 86400),
                  ('H', 3600),
                  ('M', 60),
                  ('S', 1)]

    for time_unit in time_units:
        split_str = duration_str.split(time_unit[0])
        if len(split_str) == 2:
            duration += int(split_str[0])*time_unit[1]
            duration_str = split_str[1]

    v['Duration'] = duration

    return v


# get directory for storyboards or thumbnails
# creates the directory if necessary
def get_image_directory(query, image_type):
    # get thumbnails dir
    path_name = constants.DATA_DIR + image_type + '/' + query + '/'
    imgs_path = path.dirname(path_name)
    try:
        stat(imgs_path)
    except:
        mkdir(imgs_path)

    return imgs_path + '/'


# appends query results to videos and saves thumbnails on filesystem
def get_videos(query, videos, page_token=None):
    # request videos that match query
    video_list_url = VIDEO_LIST_URL.format(API_KEY=API_KEY,
                                           QUERY=urlencode({'q': query}))
    if page_token:
        video_list_url += PAGE_PARAM.format(PAGE_TOKEN=page_token)

    r = get(video_list_url)
    results = loads(r.text)

    video_ids = list(map((lambda x: str(x['id']['videoId'])), results['items']))

    th_path = get_image_directory(query, 'thumbnails')
    sb_path = get_image_directory(query, 'storyboards')

    # download video images to their respective dirs
    fails = pool.map(partial(get_all_images, th_path=th_path, sb_path=sb_path),
                     video_ids)

    # remove videos with no storyboard found
    for v_id in filter(lambda x: x, fails):
        video_ids.remove(v_id)

    # request video details
    id_list = str(video_ids)[2:-1].replace('\'', '').replace(' u', '')
    video_details_url = VIDEO_DETAILS_URL.format(API_KEY=API_KEY,
                                                 ID_LIST=id_list)
    r = get(video_details_url)
    video_details = loads(r.text)

    # extract video details for each result
    videos += pool.map(extract_video_details, video_details['items'])

    return results.get('nextPageToken', None)


def get_duplicates(query_url):
    videos = []

    start_time = time()

    query_id = query_url[query_url.find('v=')+2:]
    video_title_url = VIDEO_DETAILS_URL.format(API_KEY=API_KEY,
                                               ID_LIST=query_id)
    r = get(video_title_url)
    video_resp = loads(r.text)

    # get details for query video
    query_video = extract_video_details(video_resp['items'][0])
    query = query_video['Title'].replace('/', '').replace('-', '').replace('?', '')

    # try to get images for query video
    th_path = get_image_directory(query, 'thumbnails')
    sb_path = get_image_directory(query, 'storyboards')
    get_all_images(query_id, th_path, sb_path)

    videos.append(query_video)

    page_token = None
    for i in range(0, 1):
        page_token = get_videos(query, videos, page_token)
        if not page_token:
            break

    pool.map(partial(st.index_video, dirname=query), videos)
             #map(lambda v: v['VideoId'].encode('utf8'), videos))

    results = st.query_index(query_id, query)
    #print(results)

    end_time = time()
    print(end_time-start_time)

    return (query, results)
