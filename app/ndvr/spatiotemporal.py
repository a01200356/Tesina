from math import ceil, exp
from array import array
from collections import defaultdict
from operator import itemgetter
from json import dump, load

import cv2

from app import constants

index = defaultdict(list)
video_details = {}
frames_per_video = {}
sb_rows = 10
sb_cols = 10
sb_frames = sb_rows*sb_cols


def get_word(frame, words, height, width):
    words.append(0)
    avg_values = []
    for i in range(0, 3):
        for j in range(0, 3):
            avg_val = frame[i*(height/3): (i+1)*(height/3)-1,
                            j*(width/3): (j+1)*(width/3)-1].mean()
            avg_values.append(avg_val)
    words[-1] += int(avg_values[4] > avg_values[1]) << 15
    words[-1] += int(avg_values[4] > avg_values[5]) << 14
    words[-1] += int(avg_values[4] > avg_values[7]) << 13
    words[-1] += int(avg_values[4] > avg_values[3]) << 12
    if len(words) <= (sb_frames-2):
        words[-1] += int(avg_values[0] > avg_values[2]) << 11
        words[-1] += int(avg_values[2] > avg_values[8]) << 10
        words[-1] += int(avg_values[8] > avg_values[6]) << 9
        words[-1] += int(avg_values[6] > avg_values[0]) << 8
    if len(words) > 2:
        words[-2] += (words[-1] >> 12) << 4
        words[-3] += words[-1] >> 12
    if len(words) == 2:
        words[-2] += (words[-1] >> 12) << 4


def get_features_from_storyboard(video_id, dirname):
    filename = constants.DATA_DIR + 'storyboards/' + dirname + '/' + video_id + '.jpg'
    sb_img = cv2.imread(filename, cv2.IMREAD_GRAYSCALE)
    if sb_img is not None:
        h = len(sb_img)/sb_rows
        w = len(sb_img[0])/sb_cols
        words = array('H')
        for i in range(0, sb_rows):
            for j in range(0, sb_cols):
                get_word(sb_img[i*h:(i+1)*h-1, j*w:(j+1)*w-1], words, h, w)
        frames_per_video[video_id] = sb_rows*sb_cols - 2
        return words[:-2]
    else:
        return get_features_from_thumbnails(video_id, dirname)


def get_features_from_thumbnails(video_id, dirname):
    thumbs_words = array('H')
    all_words = array('H')
    n_thumbnails = 0

    # obtain the words for the available thumbnails
    for i in range(0, 3):
        filename = constants.DATA_DIR + 'thumbnails/' + \
                   dirname + '/' + video_id + str(i) + '.jpg'

        th_img = cv2.imread(filename, cv2.IMREAD_GRAYSCALE)
        if th_img is not None:
            h = len(th_img)
            w = len(th_img[0])
            get_word(th_img, thumbs_words, h, w)
            n_thumbnails += 1

    # append all combinations of suffixes in words
    # to make up for the lack of available frames
    mask = pow(2, 8) - 1 << 8
    for word in thumbs_words:
        word &= mask
        for i in range(1, pow(2, 8)):
            all_words.append(word + i)

    frames_per_video[video_id] = n_thumbnails
    return all_words


def get_features_from_video(video_id, dirname):
    filename = constants.DATA_DIR + 'tmp/' + dirname + '/' + video_id
    video_stream = cv2.VideoCapture(filename)
    success, frame = video_stream.read()
    fps = int(ceil(video_stream.get(cv2.cv.CV_CAP_PROP_FPS)))
    words = array('H')
    n_keyframes = 0
    
    while success:
        frame_n = int(round(video_stream.get(1)))
        if frame_n % fps == 0:
            n_keyframes += 1
            h = len(frame)
            w = len(frame[0])
            get_word(frame, words, h, w)

        success, frame = video_stream.read()

    frames_per_video[video_id] = n_keyframes
    
    return words[:-2]


def create_video_histogram(words):
    hist = [0] * pow(2, 16)
    for word in words:
        hist[word] += 1
    return hist


def index_histogram(video_id, histogram):
    for word in range(0, len(histogram)):
        if histogram[word] > 0:
            index[str(word)].append((video_id, histogram[word]))


def index_video(video, dirname, from_storyboard=True):
    video_id = video['VideoId']

    if from_storyboard:
        words = get_features_from_storyboard(video_id, dirname)
        video_details[video_id] = video
    else:
        words = get_features_from_video(video_id, dirname)

    if words:
        hist = create_video_histogram(words)
        index_histogram(video_id, hist)
        return True
    else:
        return False


def load_video_features(dirname):
    #with open(constants.DATA_DIR + dirname + '.json', "r") as f:
    with open(constants.DATA_DIR + 'all.json', "r") as f:
        global index
        index = load(f)
        for word in index:
            for v in index[word]:
                frames_per_video[v[0]] = frames_per_video.get(v[0], 0) + v[1]


def compare_video_durations(video_id_a, video_id_b):
    duration_a = int(video_details.get(video_id_a, {}).get('Duration'))
    duration_b = int(video_details.get(video_id_b, {}).get('Duration'))

    longer = max(duration_a, duration_b)
    shorter = min(duration_a, duration_b)

    return float(shorter) / longer
    #return exp(1 - float(longer)/shorter)


def weight_scores(query_id, results, weight):
    for video_id, visual_score in results.items():
        time_score = compare_video_durations(query_id, video_id)
        results[video_id] = (weight*visual_score + (1-weight)*time_score)
        #results[video_id] = visual_score * time_score


def query_index(video_id, dirname, from_storyboard=True, weight=0.7):
    global index
    global frames_per_video
    #with open(constants.DATA_DIR + dirname + '.json', "w") as f:
    #    dump(index, f)
    #return None
    if from_storyboard:
        words = get_features_from_storyboard(video_id, dirname)
        if not words:
            words = get_features_from_thumbnails(video_id, dirname)
    else:
        words = get_features_from_video(video_id, dirname)
        load_video_features(dirname)

    if not words:
        return None

    query_hist = create_video_histogram(words)

    results = defaultdict(float)

    # histogram intersection
    for word in range(0, len(query_hist)):
        if query_hist[word] > 0:
            for video, count in index.get(str(word), []):
                if video != video_id:
                    results[video] += min(count, query_hist[word])

    q_frames = frames_per_video[video_id]
    for video in results:
        n_frames = frames_per_video[video]
        #print(results[video])
        #print(n_frames, q_frames, results[video])
        results[video] /= (n_frames + q_frames - results[video])
        #print(results[video])

    if from_storyboard:
        weight_scores(video_id, results, weight=weight)

    index = defaultdict(list)
    frames_per_video = {}
    return sorted(results.items(), key=itemgetter(1), reverse=True)
    #return filter(lambda x: x[1] > 0.1,
    #              sorted(results.items(), key=itemgetter(1), reverse=True))
