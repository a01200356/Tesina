import sys
reload(sys)
sys.setdefaultencoding("utf-8")

from flask import request, render_template

from app import app
from app.ndvr.youtube import get_duplicates

@app.route('/')
def index():
    return render_template('index.html')

@app.route('/search', methods=['POST'])
def search():
    query_title, results = get_duplicates(request.form.get('query_url'))
    return render_template('index.html',
                           query_title=query_title,
                           results=results)
