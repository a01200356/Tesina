# Near-Duplicate Web Video Detection Using Only Metadata

This is a demo for my dissertation research on near-duplicate video detection.  
The application is a reverse YouTube video search engine.  
Speed is achieved by only comparing available metadata, rather than the actual videos.  
According to the tests, the algorithm is about 61% accurate.  

## How to install
Tested on a Debian 9 server.  

### Install system requirements
 * Python 2.7
 * virtualenv
 * python-opencv

### Prepare virtual environment
 * `virtualenv -p python2 env`
 * `source env/bin/activate`
 * `pip install -r requirements.txt`
 * `cp -a /usr/lib/python2.7/dist-packages/cv* ./env/lib/python2.7/site-packages/`

### Get YouTube API key
 * https://developers.google.com/youtube/v3/getting-started
 * Paste API key in app/ndvr/youtube.py in the line that says `API_KEY = ''`

### Run
 * Move to project's root.
 * `source env/bin/activate`
 * `./run`
 * Go to the URL printed.
 * Paste the URL of a YouTube video you want to query.
 * Hopefully a list of near-duplicates should appear.
 * Tip: Try music videos or memes.
